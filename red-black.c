#include <stdio.h>
#include <stdlib.h>

enum Cor {VERMELHO, PRETO};

struct No {
    int dado;
    enum Cor cor;
    struct No *esquerda, *direita, *pai;
};

struct No* novoNo(int dado) {
    struct No* temp = (struct No*)malloc(sizeof(struct No));
    temp->dado = dado;
    temp->cor = VERMELHO;
    temp->esquerda = temp->direita = temp->pai = NULL;
    return temp;
}

void rotacaoEsquerda(struct No **raiz, struct No *x) {
    struct No *y = x->direita;
    x->direita = y->esquerda;
    if (y->esquerda != NULL)
        y->esquerda->pai = x;
    y->pai = x->pai;
    if (x->pai == NULL)
        *raiz = y;
    else if (x == x->pai->esquerda)
        x->pai->esquerda = y;
    else
        x->pai->direita = y;
    y->esquerda = x;
    x->pai = y;
}

void rotacaoDireita(struct No **raiz, struct No *x) {
    struct No *y = x->esquerda;
    x->esquerda = y->direita;
    if (y->direita != NULL)
        y->direita->pai = x;
    y->pai = x->pai;
    if (x->pai == NULL)
        *raiz = y;
    else if (x == x->pai->direita)
        x->pai->direita = y;
    else
        x->pai->esquerda = y;
    y->direita = x;
    x->pai = y;
}

void corrigirArvoreAposInsercao(struct No **raiz, struct No *z) {
    struct No *y;
    while (z != *raiz && z->pai->cor == VERMELHO) {
        if (z->pai == z->pai->pai->esquerda) {
            y = z->pai->pai->direita;
            if (y->cor == VERMELHO) {
                z->pai->cor = PRETO;
                y->cor = PRETO;
                z->pai->pai->cor = VERMELHO;
                z = z->pai->pai;
            } else {
                if (z == z->pai->direita) {
                    z = z->pai;
                    rotacaoEsquerda(raiz, z);
                }
                z->pai->cor = PRETO;
                z->pai->pai->cor = VERMELHO;
                rotacaoDireita(raiz, z->pai->pai);
            }
        } else {
            y = z->pai->pai->esquerda;
            if (y->cor == VERMELHO) {
                z->pai->cor = PRETO;
                y->cor = PRETO;
                z->pai->pai->cor = VERMELHO;
                z = z->pai->pai;
            } else {
                if (z == z->pai->esquerda) {
                    z = z->pai;
                    rotacaoDireita(raiz, z);
                }
                z->pai->cor = PRETO;
                z->pai->pai->cor = VERMELHO;
                rotacaoEsquerda(raiz, z->pai->pai);
            }
        }
    }
    (*raiz)->cor = PRETO;
}

void inserir(struct No **raiz, int dado) {
    struct No *z = novoNo(dado);
    struct No *y = NULL;
    struct No *x = *raiz;
    while (x != NULL) {
        y = x;
        if (z->dado < x->dado)
            x = x->esquerda;
        else
            x = x->direita;
    }
    z->pai = y;
    if (y == NULL)
        *raiz = z;
    else if (z->dado < y->dado)
        y->esquerda = z;
    else
        y->direita = z;
    corrigirArvoreAposInsercao(raiz, z);
}

struct No* menorNo(struct No* no) {
    while (no->esquerda != NULL)
        no = no->esquerda;
    return no;
}

struct No* sucessor(struct No *x) {
    if (x->direita != NULL)
        return menorNo(x->direita);

    struct No *y = x->pai;
    while (y != NULL && x == y->direita) {
        x = y;
        y = y->pai;
    }
    return y;
}

void excluirNo(struct No **raiz, struct No *z) {
    struct No *x, *y;

    if (z->esquerda == NULL || z->direita == NULL)
        y = z;
    else
        y = sucessor(z);

    if (y->esquerda != NULL)
        x = y->esquerda;
    else
        x = y->direita;

    if (x != NULL)
        x->pai = y->pai;

    if (y->pai == NULL)
        *raiz = x;
    else if (y == y->pai->esquerda)
        y->pai->esquerda = x;
    else
        y->pai->direita = x;

    if (y != z)
        z->dado = y->dado;

    if (y->cor == PRETO)
        corrigirArvoreAposExclusao(raiz, x);

    free(y);
}

void corrigirArvoreAposExclusao(struct No **raiz, struct No *x) {
    struct No *w;
    while (x != *raiz && x->cor == PRETO) {
        if (x == x->pai->esquerda) {
            w = x->pai->direita;
            if (w->cor == VERMELHO) {
                w->cor = PRETO;
                x->pai->cor = VERMELHO;
                rotacaoEsquerda(raiz, x->pai);
                w = x->pai->direita;
            }
            if (w->esquerda->cor == PRETO && w->direita->cor == PRETO) {
                w->cor = VERMELHO;
                x = x->pai;
            } else {
                if (w->direita->cor == PRETO) {
                    w->esquerda->cor = PRETO;
                    w->cor = VERMELHO;
                    rotacaoDireita(raiz, w);
                    w = x->pai->direita;
                }
                w->cor = x->pai->cor;
                x->pai->cor = PRETO;
                w->direita->cor = PRETO;
                rotacaoEsquerda(raiz, x->pai);
                x = *raiz;
            }
        } else {
            w = x->pai->esquerda;
            if (w->cor == VERMELHO) {
                w->cor = PRETO;
                x->pai->cor = VERMELHO;
                rotacaoDireita(raiz, x->pai);
                w = x->pai->esquerda;
            }
            if (w->direita->cor == PRETO && w->esquerda->cor == PRETO) {
                w->cor = VERMELHO;
                x = x->pai;
            } else {
                if (w->esquerda->cor == PRETO) {
                    w->direita->cor = PRETO;
                    w->cor = VERMELHO;
                    rotacaoEsquerda(raiz, w);
                    w = x->pai->esquerda;
                }
                w->cor = x->pai->cor;
                x->pai->cor = PRETO;
                w->esquerda->cor = PRETO;
                rotacaoDireita(raiz, x->pai);
                x = *raiz;
            }
        }
    }
    x->cor = PRETO;
}

int main() {
    struct No *raiz = NULL;

    inserir(&raiz, 10);
    inserir(&raiz, 20);
    inserir(&raiz, 30);
    inserir(&raiz, 40);
    inserir(&raiz, 50);
    inserir(&raiz, 25);
    excluirNo(&raiz, 10);
    excluirNo(&raiz, 30);
    excluirNo(&raiz, 50);

    return 0;
}

